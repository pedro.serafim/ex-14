import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int primeiroNumero, segundoNumero;
        boolean Igual=false, NaoIgual=false, Maior=false, Menor=false, MaiorOuIgual=false, MenorOuIgual=false;

        imprime("Mostrador de relacionamentos\n\n");

        imprime("Digite o primeiro número:  ");
        primeiroNumero = entrada.nextInt();

        imprime("Digite o segundo número: ");
        segundoNumero = entrada.nextInt();

        if(primeiroNumero == segundoNumero) Igual = true;
        if(primeiroNumero != segundoNumero) NaoIgual = true;
        if(primeiroNumero > segundoNumero) Maior = true;
        if(primeiroNumero < segundoNumero) Menor = true;
        if(primeiroNumero >= segundoNumero) MaiorOuIgual = true;
        if(primeiroNumero <= segundoNumero) MenorOuIgual = true;

        imprime("Igual = "+Igual);
        imprime("Não Igual = "+NaoIgual);
        imprime("Maior = "+Maior);
        imprime("Menor = "+Menor);
        imprime("Maior ou igual = "+MaiorOuIgual);
        imprime("Menor ou igual = "+MenorOuIgual);

    }
    public static void imprime(String mensagem){
        System.out.println(mensagem);
    }
}